package webui.LoginOut;

import org.testng.annotations.Test;

import com.autowebui.PageAction.LoginAction;
import com.autowebui.PageAction.LoginOutAction;
import com.autowebui.utils.Assertion;
import com.autowebui.utils.YamlDataHelper;

public class LoginOut {
	String src = "main/java/com/autowebui/config/config.yaml";
	Object[][] files = YamlDataHelper.yamlData(src);
	private String username = (String) files[1][0];
	private String pwd = (String) files[1][1];

	@Test
	public void loginOut() {
		new LoginAction("chrome", "http://run.susonoffice.top", username, pwd);
		LoginOutAction action=new LoginOutAction();
		action.sleep(3);
		Assertion.init();
		Assertion.VerityString(action.Page(),"登录：","登出校验");
		Assertion.VerityError();
	}
	@Test
	public void loginOutIE() {
		new LoginAction("ie", "http://run.susonoffice.top", username, pwd);
		LoginOutAction action=new LoginOutAction();
		action.sleep(3);
		Assertion.init();
		Assertion.VerityString(action.Page(),"登录：","登出校验");
		Assertion.VerityError();
	}
}

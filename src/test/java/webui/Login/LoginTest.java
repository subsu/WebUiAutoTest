package webui.Login;

import java.io.IOException;
import org.dom4j.DocumentException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.autowebui.PageAction.LoginAction;
import com.autowebui.utils.Assertion;
import com.autowebui.utils.YamlDataHelper;

public class LoginTest {
	@DataProvider(name="login")
	public Object[][] testData() {
		String src="test/java/webui/Login/login.yaml";
		Object[][] files=YamlDataHelper.yamlData(src);
		return files;
	}
	
	@Test(description="Chrome浏览器-登陆校验",dataProvider="login")
	public void login(String username,String pwd,String msg) throws IOException, DocumentException {
		LoginAction action=new LoginAction("chrome","http://run.susonoffice.top",username,pwd);
		action.sleep(3);
		Assertion.init();
		Assertion.VerityTextPresent(msg,msg);
		Assertion.VerityError();
	}
	@Test(description="IE浏览器-登陆校验",dataProvider="login")
	public void loginIE(String username,String pwd,String msg) {
		LoginAction action=new LoginAction("ie","http://run.susonoffice.top",username,pwd);
		action.sleep(3);
		Assertion.init();
		Assertion.VerityTextPresent(msg,msg);
		Assertion.VerityError();
	}
}

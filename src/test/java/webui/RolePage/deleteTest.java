package webui.RolePage;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.autowebui.PageAction.LoginAction;
import com.autowebui.PageAction.RoleAction;
import com.autowebui.utils.Assertion;
import com.autowebui.utils.YamlDataHelper;

public class deleteTest {
	String src = "main/java/com/autowebui/config/config.yaml";
	Object[][] files = YamlDataHelper.yamlData(src);
	private String username = (String) files[1][0];
	private String pwd = (String) files[1][1];
	
	@DataProvider(name="delRole")
	public Object[][] testData() {
		String src="test/java/webui/RolePage/delCase.yaml";
		Object[][] files=YamlDataHelper.yamlData(src);
		return files;
	}
	
	@Test(description="删除角色",dataProvider="delRole")
	public void deleteRole(String driver,String name,boolean co,String msg) {
		new LoginAction(driver, "http://run.susonoffice.top", username, pwd);
		RoleAction action=new RoleAction();
		action.del(name,co);
		Assertion.init();
		if(co) {//判断是否取消按钮测试
			Assertion.VerityTextPresent("操作成功",msg);
		}else {
			Assertion.VerityTextPresent(name,msg);
		}
		Assertion.VerityError();
	}

}

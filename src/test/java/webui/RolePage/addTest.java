package webui.RolePage;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.autowebui.PageAction.LoginAction;
import com.autowebui.PageAction.RoleAction;
import com.autowebui.utils.Assertion;
import com.autowebui.utils.YamlDataHelper;

public class addTest {
	String src = "main/java/com/autowebui/config/config.yaml";
	Object[][] files = YamlDataHelper.yamlData(src);
	private String username = (String) files[1][0];
	private String pwd = (String) files[1][1];
	
	@DataProvider(name="addRole")
	public Object[][] testData() {
		String src="test/java/webui/RolePage/addCase.yaml";
		Object[][] files=YamlDataHelper.yamlData(src);
		return files;
	}
	
	@Test(description="新增角色",dataProvider="addRole")
	public void addRole(String drivers,String name,String power,String order,boolean play,String ps,String menu,String msg) {
		new LoginAction(drivers, "http://run.susonoffice.top", username, pwd);
		RoleAction action=new RoleAction();
		action.add(name,power,order,play,ps,menu);
		action.submit();
		Assertion.init();
		Assertion.VerityTextPresent("操作成功",msg);
		Assertion.VerityError();
	}

}

package webui;

import org.testng.annotations.Test;

import com.autowebui.utils.YamlDataHelper;

import java.util.Map;

public class TestYamlData extends YamlDataHelper {

	@Test(dataProvider = "yamlDataMethod")
	public void testYamlData(Map<String, String> param) {
		System.out.println(param.get("name") + "\t" + param.get("passwd"));
	}
}
package com.autowebui.PageObject;

import com.autowebui.utils.BasePage;
import com.autowebui.utils.Locator;
import com.autowebui.utils.Locator.ByType;

public class RolePage extends BasePage {
	public RolePage() {

	}

	public Locator add() {
		Locator locator = getLocator("//*[@id=\'toolbar\']/a[1]", ByType.xpath);
		return locator;
	}

	public Locator edit() {
		Locator locator = getLocator("//*[@id=\'toolbar\']/a[2]", ByType.xpath);
		return locator;
	}

	public Locator del() {
		Locator locator = getLocator("//*[@id=\'toolbar\']/a[3]", ByType.xpath);
		return locator;
	}

	public Locator name() {// 角色名称
		Locator locator = getLocator("//*[@id='roleName']", ByType.xpath);
		return locator;
	}

	public Locator power() {// 权限字符
		Locator locator = getLocator("//*[@id=\'roleKey\']", ByType.xpath);
		return locator;
	}

	public Locator order() {// 显示顺序
		Locator locator = getLocator("//*[@id=\'roleSort\']", ByType.xpath);
		return locator;
	}
	
	public Locator play() {// 显示状态
		Locator locator = getLocator("//*[@id=\'form-role-add\']/div[4]/div/div", ByType.xpath);
		return locator;
	}

	public Locator ps() {// 备注
		Locator locator = getLocator("//*[@id=\'remark\']", ByType.xpath);
		return locator;
	}

	public Locator meun_mgr() {// 系统管理权限
		Locator locator = getLocator("//*[@id='menuTrees_1_check']", ByType.xpath);
		return locator;
	}

	public Locator meun_mir() {// 系统监控权限
		Locator locator = getLocator("//*[@id=\'menuTrees_59_check\']", ByType.xpath);
		return locator;
	}

	public Locator meun_tool() {// 系统工具权限
		Locator locator = getLocator("//*[@id=\'menuTrees_74_check\']", ByType.xpath);
		return locator;
	}

	public Locator meun() {//系统管理
		Locator locator = getLocator("//*[@id=\"side-menu\"]/li[4]", ByType.xpath);
		return locator;
	}
	public Locator meun_role() {//菜单角色
		Locator locator = getLocator("//*[@id=\'side-menu\']/li[4]/ul/li[2]/a", ByType.xpath);
		return locator;
	}
	public Locator load() {//页面加载
		Locator locator = getLocator("/html/body/div[1]/div/div[2]/div[1]/div[2]/div[4]/div[1]", ByType.xpath);
		return locator;
	}
	public Locator submit() {//提交
		Locator locator =getLocator("#layui-layer1 > div.layui-layer-btn.layui-layer-btn- > a.layui-layer-btn0",ByType.cssSelector);
		return locator;
	}
	public Locator closed() {//关闭
		Locator locator =getLocator("#layui-layer1 > div.layui-layer-btn.layui-layer-btn- > a.layui-layer-btn1",ByType.cssSelector);
		return locator;
	}
	public Locator iframe1() {
		Locator locator=getLocator("//*[@id=\"content-main\"]/iframe[2]",ByType.xpath);
		return locator;
	}
	public Locator iframe2() {
		Locator locator=getLocator("//*[@id='layui-layer-iframe1']",ByType.xpath);
		return locator;
	}
	public Locator search() {//搜索输入框
		Locator locator=getLocator("//*[@id=\'role-form\']/div/ul/li[1]/input",ByType.xpath);
		return locator;
	}
	public Locator sbtn() {//搜索按钮
		Locator locator=getLocator("//*[@id='role-form']/div/ul/li[5]/a[1]",ByType.xpath);
		return locator;
	}
	public Locator delete() {//列表中的删除
		Locator locator=getLocator("//*[@id=\'bootstrap-table\']/tbody/tr[1]/td[8]/a[3]",ByType.xpath);
		return locator;
	}
	public Locator cbtn() {//确认删除
		Locator locator=getLocator("//*[@id=\'layui-layer1\']/div[3]/a[1]",ByType.xpath);
		return locator;
	}
	public Locator cabtn() {//取消删除
		Locator locator=getLocator("//*[@id=\'layui-layer1\']/div[3]/a[2]",ByType.xpath);
		return locator;
	}
}

/**
 * @author Suson
 * @param
 */
package com.autowebui.PageObject;

import com.autowebui.utils.BasePage;
import com.autowebui.utils.Locator;
import com.autowebui.utils.Locator.ByType;

import org.apache.log4j.Logger;

public class baidu extends BasePage {
	public baidu(String type, String url) {
		// TODO 自动生成的构造函数存根
		browser(type);
		url(url);
	}

	// 输入关键词
	public void kw_sendkes(String s) {
		Locator locator=getLocator("kw",ByType.id);
		input(locator, s);
	}

	// 点击“搜索”按钮
	public void su_click() {
		Locator locator=getLocator("sus",ByType.id);
		actionClick(locator);
	}

}
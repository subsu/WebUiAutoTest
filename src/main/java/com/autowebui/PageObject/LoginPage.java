package com.autowebui.PageObject;

import com.autowebui.utils.BasePage;
import com.autowebui.utils.Locator;
import com.autowebui.utils.Locator.ByType;

public class LoginPage extends BasePage {
	public LoginPage() {
		// TODO 自动生成的构造函数存根
	}
	public Locator username()//用户名
	 {
	   Locator locator=getLocator("//*[@id='signupForm']/input[1]",ByType.xpath);
	   return locator;
	 }
	public Locator psw()//密码
	 {
	   Locator locator=getLocator("//*[@id='signupForm']/input[2]",ByType.xpath);
	   return locator;
	 }
	public Locator login() {//登陆
		Locator locator=getLocator("//*[@id=\'btnSubmit\']",ByType.xpath);
		return locator;
	}
	public Locator name() {//获取登陆后界面的用户名
		Locator locator=getLocator("//*[@id=\'side-menu\']/li[2]/div/div/p",ByType.xpath);
		return locator;
	}
}

package com.autowebui.PageObject;

import com.autowebui.utils.BasePage;
import com.autowebui.utils.Locator;
import com.autowebui.utils.Locator.ByType;

public class LoginOutPage extends BasePage {
	public LoginOutPage(){
		
	}
	public Locator menu() {
		Locator menu=getLocator("//*[@id=\'page-wrapper\']/div[1]/nav/ul/li[3]/a",ByType.xpath);
		return menu;
	}
	public Locator loginOut() {
		Locator loginOut=getLocator("//*[@id=\'page-wrapper\']/div[1]/nav/ul/li[3]/ul/li[4]/a",ByType.xpath);
		return loginOut;
	}
	public Locator page() {
		Locator locator=getLocator("//*[@id=\'signupForm\']/h4",ByType.xpath);
		return locator;
	}
}

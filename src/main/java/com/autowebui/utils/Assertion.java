package com.autowebui.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.openqa.selenium.WebDriver;

import com.autowebui.utils.BasePage;

/**
 * 断言类，用于设置检查点的方法,并失败后截图
 * 
 * @author Suson
 *
 */
public class Assertion extends BasePage {
	// 收集断言异常用于报表日志展示
	public static List<Error> errors = new ArrayList<Error>();
	// 收集断言信息文本，用于报表展示
	public static List<String> assertInfolList = new ArrayList<String>();
	private static Log log = new Log(Assertion.class);

	public static String formatDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HHmmssSSS");
		return formatter.format(date).toString();
	}

	private static void snapshotInfo() {
		WebDriver driver = BasePage.driver;
		ScreenShot screenShot = new ScreenShot(driver);
		// 设置截图名字
		Date nowDate = new Date();
		screenShot.setscreenName(Assertion.formatDate(nowDate));
		screenShot.takeScreenshot();
	}

	/**
	 * 验证actual实际值是否包含预期值exceptStr
	 * 
	 * @param actual    实际值
	 * @param exceptStr 预期值
	 * @author Suson
	 */
	public static void VerityCationString(String actual, String exceptStr) {
		String verityStr = "Assert验证：{" + "实际值：" + actual + "," + "预期值：" + exceptStr + "} 实际值是否包含预期值";
		Boolean flagBoolean = actual.contains(exceptStr);
		log.info(flagBoolean.toString());
		try {
			Assert.assertTrue(flagBoolean);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}

	}

	/**
	 * 验证actual实际值是否包含预期值exceptStr
	 * 
	 * @param actual    实际值
	 * @param exceptStr 预期值
	 * @param Message   验证中文描述
	 * @author Suson
	 */
	public void VerityCationString(String actual, String exceptStr, String Message) {
		String verityStr = "Assert验证：{" + "实际值：" + actual + "," + "预期值：" + exceptStr + "} 实际值是否包含预期值";
		Boolean flagBoolean = actual.contains(exceptStr);
		log.info(Message + ":" + verityStr);
		try {
			Assert.assertTrue(flagBoolean);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}

	}

	/**
	 * 验证实际值actual与预期值exceptStr是否相等
	 * 
	 * @param actual    实际值
	 * @param exceptStr 预期值
	 * @author Suson
	 */
	public static void VerityString(String actual, String exceptStr) {
		String verityStr = "Assert验证：{" + "实际值：" + actual + "," + "预期值" + exceptStr + "} 实际值与预期值是否一致";
		log.info(verityStr);
		try {
			Assert.assertEquals(actual, exceptStr);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}
	}

	/**
	 * 验证实际值actual与预期值exceptStr是否相等
	 * 
	 * @param actual    实际值
	 * @param exceptStr 预期值
	 * @param Message   验证中文描述
	 * @author Suson
	 */
	public static void VerityString(String actual, String exceptStr, String Message) {
		String verityStr = "Assert验证：{" + "实际值" + actual + "," + "预期值" + exceptStr + "} 实际值与预期值是否一致";
		log.info(Message + ":" + verityStr);
		try {
			Assert.assertEquals(actual, exceptStr);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}
	}

	/**
	 * 验证实际值actual与预期值exceptStr是否不相等
	 * 
	 * @param actual    实际值
	 * @param exceptStr 预期值
	 * @param Message   验证中文描述
	 * @author Suson
	 */
	public static void VerityNotString(String actual, String exceptStr, String Message) {
		String verityStr = "Assert验证：{" + "实际值" + actual + "," + "预期值" + exceptStr + "} 实际值与预期值是否不相等";
		log.info(Message + ":" + verityStr);
		try {
			Assert.assertNotEquals(actual, exceptStr);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}
	}

	/**
	 * 验证布尔值是否与预期一致
	 * 
	 * @param actual  实际值
	 * @param except  预期值
	 * @param message 信息
	 */
	public static void VerityBoolean(Boolean actual, Boolean except, String message) {

		String verityStr = "Assert验证：{" + "实际值：" + actual + "," + "预期值：" + except + "} 实际值与预期值是否一致";
		log.info(message + ":" + verityStr);
		try {
			Assert.assertEquals(actual, except);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}
	}

	/**
	 * 验证页面是否出现某文本exceptStr
	 * 
	 * @param exceptStr 预期值
	 * @author Suson
	 */
	public static void VerityTextPresent(String exceptStr) {
		String verityStr = "【Assert验证】:" + "页面是否出现" + "【" + "预期值：" + exceptStr + "】" + "字符串";
		Boolean flag = false;
		log.info(verityStr);
		try {
			exceptStr = "//*[contains(text(),'" + exceptStr + "')]";
			log.info("定位信息：" + exceptStr);
			driver.findElements(By.xpath(exceptStr));
			if (driver.findElements(By.xpath(exceptStr)).size() > 0) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (NoSuchElementException e) {
			flag = false;
			BasePage.noSuchElementExceptions.add(e);
			e.printStackTrace();
		}
		try {
			Assert.assertTrue(flag);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}

	}

	/**
	 * 验证页面是否出现某文本exceptStr
	 * 
	 * @param exceptStr 预期值
	 * @param Message   验证中文描述
	 * @author Suson
	 */
	public static void VerityTextPresent(String exceptStr, String Message) {
		String verityStr = "【Assert验证】:" + "页面是否出现" + "【" + "预期值：" + exceptStr + "】" + "字符串";
		Boolean flag = false;
		log.info(Message + ":" + verityStr);
		try {
			exceptStr = "//*[contains(text(),'" + exceptStr + "')]";
			System.out.println(exceptStr);
			List<WebElement> webElements = driver.findElements(By.xpath(exceptStr));
			if (webElements.size() > 0) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (NoSuchElementException e) {
			// TODO: handle exception
			flag = false;
			BasePage.noSuchElementExceptions.add(e);
			e.printStackTrace();
		}
		try {
			Assert.assertTrue(flag);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}

	}

	/**
	 * 验证页面是否没有出现某文本exceptStr
	 * 
	 * @param exceptStr 预期值
	 * @author Suson
	 */
	public static void VerityNotTextPresent(String exceptStr) {
		String verityStr = "【Assert验证】:" + "页面是否没有出现" + "【" + "预期值：" + exceptStr + "】" + "字符串";
		Boolean flag = false;
		log.info(verityStr);
		try {
			exceptStr = "//*[contains(.,'" + exceptStr + "')]";
			driver.findElement(By.xpath(exceptStr));
			flag = false;
		} catch (NoSuchElementException e) {
			// TODO: handle exception
			flag = true;
		}
		try {
			Assert.assertTrue(flag);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}

	}

	/**
	 * 验证页面是否没有出现某文本exceptStr
	 * 
	 * @param exceptStr 预期值
	 * @param Message   验证中文描述
	 * @author Suson
	 */
	public static void VerityNotTextPresent(String exceptStr, String Message) {
		String verityStr = "【Assert验证】:" + "页面是否没有出现" + "【" + "预期值：" + exceptStr + "】" + "字符串";
		Boolean flag = false;
		log.info(Message + ":" + verityStr);
		try {
			exceptStr = "//*[contains(.,'" + exceptStr + "')]";
			driver.findElement(By.xpath(exceptStr));
			flag = false;
			System.out.println(flag);
		} catch (NoSuchElementException e) {
			// TODO: handle exception
			flag = true;
			System.out.println(flag);
		}
		try {
			Assert.assertTrue(flag);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}

	}

	/**
	 * 验证页面是否出现某文本--精确匹配
	 * 
	 * @param exceptStr 预期值 预期值
	 * @author Suson
	 */
	public static void VerityTextPresentPrecision(String exceptStr) {
		String verityStr = "【Assert验证】:" + "页面是否出现" + "【" + "预期值：" + exceptStr + "】" + "字符串";
		Boolean flag = false;
		log.info(verityStr);
		try {
			exceptStr = "//*[text()=\"" + exceptStr + "\"]";
			System.out.println(exceptStr);
			driver.findElement(By.xpath(exceptStr));
			flag = true;
		} catch (NoSuchElementException e) {
			// TODO: handle exception
			flag = false;
			BasePage.noSuchElementExceptions.add(e);
			e.printStackTrace();
		}
		System.out.println(false);
		try {
			Assert.assertTrue(flag);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}

	}

	/**
	 * 验证页面是否出现某文本--精确匹配
	 * 
	 * @param exceptStr 预期值
	 * @param Message   验证中文描述
	 * @author Suson
	 */
	public static void VerityTextPresentPrecision(String exceptStr, String Message) {
		String verityStr = "【Assert验证】:" + "页面是否出现" + "【" + "预期值：" + exceptStr + "】" + "字符串";
		Boolean flag = false;
		log.info(Message + ":" + verityStr);
		try {
			exceptStr = "//*[text()=\"" + exceptStr + "\"]";
			WebElement webElement = driver.findElement(By.xpath(exceptStr));
			System.out.println(exceptStr);
			flag = true;
		} catch (NoSuchElementException e) {
			// TODO: handle exception
			flag = false;
		}
		System.out.println(flag);
		try {
			Assert.assertTrue(flag);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}

	}

	/**
	 * 验证页面是否没有出现某文本--精确匹配
	 * 
	 * @param exceptStr 预期值
	 * @author Suson
	 */
	public static void VerityNotTextPresentPrecision(String exceptStr) {
		String verityStr = "【Assert验证】:" + "页面是否没有出现" + "【" + "预期值：" + exceptStr + "】" + "字符串";
		Boolean flag = false;
		log.info(verityStr);
		try {
			exceptStr = "//*[text()=\"" + exceptStr + "\"]";
			System.out.println(exceptStr);
			driver.findElement(By.xpath(exceptStr));
			flag = false;
		} catch (NoSuchElementException e) {
			// TODO: handle exception
			flag = true;
			BasePage.noSuchElementExceptions.add(e);
			e.printStackTrace();
			/// AssertFailedLog();
		}
		System.out.println(false);
		try {
			Assert.assertTrue(flag);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}

	}

	/**
	 * 验证是页面否没有出现某文本---精确匹配
	 * 
	 * @param exceptStr 预期值
	 * @param Message   验证中文描述
	 * @author Suson
	 */
	public static void VerityNotTextPresentPrecision(String exceptStr, String Message) {
		String verityStr = "【Assert验证】:" + "页面是否没有出现" + "【" + "预期值：" + exceptStr + "】" + "字符串";
		Boolean flag = false;
		log.info(Message + ":" + verityStr);
		try {
			exceptStr = "//*[text()=\"" + exceptStr + "\"]";
			System.out.println(exceptStr);
			driver.findElement(By.xpath(exceptStr));
			flag = false;
		} catch (NoSuchElementException e) {
			// TODO: handle exception
			flag = true;
		}
		System.out.println(flag);
		try {
			Assert.assertTrue(flag);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}

	}

	/**
	 * 验证浏览器标题是否与预期值exceptTitle相同
	 * 
	 * @param exceptTitle 预期标题
	 * @author Suson
	 */
	public static void VerityTitle(String exceptTitle) {

		String title = driver.getTitle();
		String verityStr = "Assert验证:页面title是否与预期值一致{" + "实际网页标题：" + title + "," + "预期网页标题：" + exceptTitle + "}";
		log.info(verityStr);
		try {
			Assert.assertEquals(title, exceptTitle);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			e.printStackTrace();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}
	}

	/**
	 * 验证浏览器标题是否与预期值exceptTitle相同
	 * 
	 * @param exceptTitle 预期标题
	 * @param Message     验证中文描述
	 * @author Suson
	 */
	public static void VerityTitle(String exceptTitle, String Message) {

		String title = driver.getTitle();
		String verityStr = "Assert验证:页面title是否与预期值一致{" + "实际网页标题：" + title + "," + "预期网页标题：" + exceptTitle + "}";
		log.info(Message + ":" + verityStr);
		try {
			Assert.assertEquals(title, exceptTitle);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			e.printStackTrace();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}
	}

	/**
	 * 验证某元素文本值是否与预期值exceptText一样
	 * 
	 * @param locator    元素定位信息
	 * @param exceptText 预期文本值
	 * @author Suson
	 */
	public static void VerityText(Locator locator, String exceptText) {
		BasePage action = new BasePage();
		WebElement webElement = action.findElement(locator);
		String text = webElement.getText();
		String verityStr = "Assert验证：某文本值是否与预期值一致{" + "实际值：" + text + "," + "预期值：" + exceptText + "}";
		log.info(verityStr);
		try {
			Assert.assertEquals(text, exceptText);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertPassLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}
	}

	/**
	 * 验证某元素文本值是否与预期值exceptText一样
	 * 
	 * @param locator    元素定位信息
	 * @param exceptText 预期文本值
	 * @param Message    验证中文描述
	 * @author Suson
	 */
	public static void VerityText(Locator locator, String exceptText, String Message) {
		BasePage action = new BasePage();
		WebElement webElement = action.findElement(locator);
		String text = webElement.getText();
		String verityStr = "Assert验证：某文本值是否与预期值一致{" + "实际值：" + text + "," + "预期值:" + exceptText + "}";
		log.info(Message + ":" + verityStr);
		try {
			Assert.assertEquals(text, exceptText);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertPassLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}
	}

	/**
	 * 验证某元素某个属性值与预期值exceptAttributeValue 一样
	 * 
	 * @param locator              元素定位信息
	 * @param AttributeName        元素属性名
	 * @param exceptAttributeValue 预期值
	 * @author Suson
	 */
	public static void VerityAttribute(Locator locator, String AttributeName, String exceptAttributeValue) {
		BasePage action = new BasePage();
		WebElement webElement = action.findElement(locator);
		String attribute = webElement.getAttribute(AttributeName);
		String verityStr = "Assert验证：某属性值是否与预期值一致{" + "实际属性值：" + attribute + "," + "预期属性值：" + exceptAttributeValue
				+ "}";
		try {
			Assert.assertEquals(attribute, exceptAttributeValue);
			log.info(verityStr);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}
	}

	/**
	 * 验证某元素某个属性值与预期值exceptAttributeValue 一样
	 * 
	 * @param locator              元素定位信息
	 * @param AttributeName        元素属性名
	 * @param exceptAttributeValue 预期值
	 * @param Message              验证中文描述
	 * @author Suson
	 */
	public static void VerityAttribute(Locator locator, String AttributeName, String exceptAttributeValue,
			String Message) {
		BasePage action = new BasePage();
		WebElement webElement = action.findElement(locator);
		String attribute = webElement.getAttribute(AttributeName);
		String verityStr = "Assert验证：某属性值是否与预期值一致{" + "实际属性值：" + attribute + "," + "预期属性值：" + exceptAttributeValue
				+ "}";
		log.info(Message + ":" + verityStr);
		try {
			Assert.assertEquals(attribute, exceptAttributeValue);
			log.info(verityStr);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}
	}

	/**
	 * 验证某输入框是否不可编辑
	 * 
	 * @param locator 元素定位信息
	 * @author Suson
	 */
	public static void VertityNoEdit(Locator locator) {
		Boolean status = false;
		BasePage action = new BasePage();
		WebElement webElement = action.findElement(locator);
		String value1 = webElement.getAttribute("disabled");
		String value2 = webElement.getAttribute("readOnly");
		String verityStr = "【Assert验证】:文本框是否不可编辑{" + "实际值：" + status.toString() + "，" + "预期值：false" + "}";
		if (value1.equals("true")) {
			status = true;
		} else if (value2.equals("true")) {
			status = true;
		} else {
			status = false;
		}
		log.info(verityStr);
		try {
			Assert.assertTrue(status);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}

	}

	/**
	 * 验证某输入框是否不可编辑
	 * 
	 * @param locator 元素定位信息
	 * @param Message 验证中文描述
	 * @author Suson
	 */
	public static void VertityNoEdit(Locator locator, String Message) {
		Boolean status = false;
		BasePage action = new BasePage();
		WebElement webElement = action.findElement(locator);
		String value1 = webElement.getAttribute("disabled");
		String value2 = webElement.getAttribute("readOnly");
		if (value1.equals("true")) {
			status = true;
		} else if (value2.equals("true")) {
			status = true;
		} else {
			status = false;
		}
		String verityStr = "【Assert验证】:文本框是否不可编辑{" + "实际值：" + status.toString() + "，" + "预期值：false" + "}";
		log.info(Message + ":" + verityStr);
		try {
			Assert.assertTrue(status);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}

	}

	/**
	 * 验证某元素可编辑
	 * 
	 * @param locator 元素定位信息
	 * @author Suson
	 */
	public static void VertityEdit(Locator locator) {
		Boolean status = false;
		BasePage action = new BasePage();
		WebElement webElement = action.findElement(locator);
		String value1 = webElement.getAttribute("disabled");
		String value2 = webElement.getAttribute("readOnly");
		String verityStr = "【Assert验证】:文本框是否可编辑{" + "实际值：" + status.toString() + "，" + "预期值：true" + "}";
		if (value1.equals("false")) {
			status = true;
		} else if (value2.equals("false")) {
			status = true;
		} else {
			status = false;
		}
		// AssertLog("true", status.toString());
		log.info(verityStr);
		try {
			Assert.assertTrue(status);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}
	}

	/**
	 * 验证某元素可编辑
	 * 
	 * @param locator 元素定位信息
	 * @param Message 验证中文描述
	 * @author Suson
	 */
	public static void VertityEdit(Locator locator, String Message) {
		Boolean status = false;
		BasePage action = new BasePage();
		WebElement webElement = action.findElement(locator);
		String value1 = webElement.getAttribute("disabled");
		String value2 = webElement.getAttribute("readOnly");
		String verityStr = "【Assert验证】:文本框是否可编辑{" + "实际值：" + status.toString() + "，" + "预期值：true" + "}";
		if (value1.equals("false")) {
			status = true;
		} else if (value2.equals("false")) {
			status = true;
		} else {
			status = false;
		}
		// AssertLog("true", status.toString());
		log.info(Message + ":" + verityStr);
		try {
			Assert.assertTrue(status);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}
	}

	// 还要修改
	/**
	 * 验证alert对话框提示信息是否与预期值一致
	 * 
	 * @param expectAlertText alert 提示框预期信息
	 * @author Suson
	 */
	public static void VerityAlertText(String expectAlertText) {
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText();
		String verityStr = "【Assert验证】:弹出的对话框的文本内容是否一致{" + alertText + "," + expectAlertText + "}";
		log.info("【Assert验证】:弹出的对话框的文本内容是否一致{" + "实际值：" + alertText + "," + "预期值" + expectAlertText + "}");
		try {
			Assert.assertEquals(alertText, expectAlertText);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}

	}

	/**
	 * 验证alert对话框提示信息是否与预期值一致
	 * 
	 * @param expectAlertText alert 提示框预期信息
	 * @param Message         验证中文描述
	 * @author Suson
	 */
	public static void VerityAlertText(String expectAlertText, String Message) {
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText();
		String verityStr = "【Assert验证】:弹出的对话框的文本内容是否一致{" + "实际值：" + alertText + "," + "预期值：" + expectAlertText + "}";
		log.info(Message + ":" + verityStr);
		try {
			Assert.assertEquals(alertText, expectAlertText);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}

	}

	/**
	 * 验证当前URL是否与预期Url一致
	 * 
	 * @param expectURL 预期URL
	 * @author Suson
	 */
	public static void VerityURL(String expectURL) {
		String url = driver.getCurrentUrl();
		String verityStr = "【Assert验证】:URL是否与预期的一致{" + "实际值：" + url + "," + "预期值：" + expectURL + "}";
		log.info(verityStr);
		try {
			Assert.assertEquals(url, expectURL);
			AssertPassLog();
			assertInfolList.add(verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(verityStr + ":failed");
		}
	}

	/**
	 * 验证当前URL是否与预期Url一致
	 * 
	 * @param expectURL 预期URL
	 * @param Message   验证中文描述
	 * @author Suson
	 */
	public static void VerityURL(String expectURL, String Message) {
		String url = driver.getCurrentUrl();
		String verityStr = "【Assert验证】:URL是否与预期的一致{" + "实际值：" + url + "," + "预期值：" + expectURL + "}";
		log.info(Message + ":" + verityStr);
		try {
			Assert.assertEquals(url, expectURL);
			AssertPassLog();
			assertInfolList.add(Message + verityStr + ":pass");
		} catch (Error e) {
			// TODO: handle exception
			AssertFailedLog();
			Assertion.snapshotInfo();
			errors.add(e);
			assertInfolList.add(Message + verityStr + ":failed");
		}
	}

	// 断言成功日志内容
	private static void AssertPassLog() {
		log.info("【Assert验证  pass!】");
	}

	// 断言失败日志内容
	private static void AssertFailedLog() {
		log.error("【Assert验证  failed!】");
	}

	// 断言日志内容
	private static void AssertLog(String str1, String str2) {
		log.info("【Assert验证】:" + "判断[比较]" + "{" + str1 + "," + str2 + "}" + "是否一致[相等]");
	}

	public static void init() {
		errors.clear();
		assertInfolList.clear();
	}

	public static void VerityError() {
		tearDown();
		Assert.assertEquals(errors.size(), 0, assertInfolList.toString());
		// 有找不到元素的异常也认为用例失败
	}

}

package com.autowebui.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import com.autowebui.utils.Driver;
import com.autowebui.utils.Locator.*;
import com.autowebui.utils.Log;
import com.autowebui.utils.ScreenShot;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

/**
 * @ClassName: BasePage
 * @Description: 重新封装api常用方法
 * @author Suson
 * @date 2020年1月22日
 *
 */
public class BasePage {
	public static ArrayList<Exception> noSuchElementExceptions = new ArrayList<Exception>();
	public static WebDriver driver;
	public Log log = new Log(this.getClass().getSuperclass());

	private String formatDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HHmmssSSS");
		return formatter.format(date).toString();
	}

	// 初始化浏览器
	public void browser(String type) {
		Driver browsers = new Driver();
		log.info("打开浏览器"+type);
		this.driver = browsers.getWebDriver(type);
	}

	/**
	 * @Title: element @Description: 封装根据定位方法获取对象 @param @param by @param @return
	 *         driver.findElement(by) 参数 @return WebElement 返回类型 @throws
	 */
	public WebElement element(By by) {
		// 封装根据定位方法获取对象
		try {
			driver.findElement(by);
		} catch (Exception e) {
			log.info(e.getMessage() + "对象不存在");
		}
		return driver.findElement(by);
	}

	/**
	 * @Title: tearDown @Description: 测试执行完自行关闭浏览器 @param 参数 @return void
	 *         返回类型 @throws
	 */
	@AfterTest
	public static void tearDown() {
		if (!driver.toString().contains("null")) {
			driver.quit();
		}
	}

	/**
	 * 从对象库获取定位信息
	 * 
	 * @param elem对象和type类型获取对象
	 * @return
	 * @throws IOException
	 */
	public Locator getLocator(String elem, ByType type) {
		Locator locator;
		locator = new Locator(elem, type);
		return locator;

	}

	public WebElement findElement(final Locator locator) {
		/**
		 * 查找某个元素等待几秒
		 */
		WebElement webElement = null;
		try {
			webElement = (new WebDriverWait(driver, 10)).until(new ExpectedCondition<WebElement>() {

				@Override
				public WebElement apply(WebDriver driver) {
					// TODO 自动生成的方法存根
					WebElement element = null;
					element = getElement(locator);
					return element;
				}
			});
			return webElement;
		} catch (NoSuchElementException e) {
			// TODO: handle exception
			log.info("无法定位页面元素");
			e.printStackTrace();
			ScreenShot screenShot = new ScreenShot(driver);
			// 设置截图名字
			Date nowDate = new Date();
			screenShot.setscreenName(this.formatDate(nowDate));
			screenShot.takeScreenshot();
			return webElement;
		} catch (TimeoutException e) {
			// TODO: handle exception
			log.info("超时无法定位页面元素");
			e.printStackTrace();
			ScreenShot screenShot = new ScreenShot(driver);
			// 设置截图名字
			Date nowDate = new Date();
			screenShot.setscreenName(this.formatDate(nowDate));
			screenShot.takeScreenshot();
			return webElement;
		} catch (ElementNotVisibleException e) {
			// TODO: handle exception
			log.info("超时无法定位页面元素");
			e.printStackTrace();
			ScreenShot screenShot = new ScreenShot(driver);
			// 设置截图名字
			Date nowDate = new Date();
			screenShot.setscreenName(this.formatDate(nowDate));
			screenShot.takeScreenshot();
			return webElement;
		} catch (Exception e) {
			log.info("其他异常" + e.getMessage());
			e.printStackTrace();
			ScreenShot screenShot = new ScreenShot(driver);
			// 设置截图名字
			Date nowDate = new Date();
			screenShot.setscreenName(this.formatDate(nowDate));
			screenShot.takeScreenshot();
			return webElement;
		}
	}

	
	/**
	* @Title: Waitformax
	* @Description: 隐式等待
	* @param @param t    参数
	* @return void    返回类型
	* @throws
	*/
	public void Waitformax(int t) {
		driver.manage().timeouts().implicitlyWait(t, TimeUnit.SECONDS);
	}

	/**
	 * @Title: getElement @Description: 通过locator定位信息获取元素 @param @param
	 *         locator @return WebElement 返回类型 @throws
	 */
	public WebElement getElement(Locator locator) {
		WebElement webElement;
		switch (locator.getBy()) {//获取定位方式
		case xpath:
			webElement = driver.findElement(By.xpath(locator.getElement()));
			break;
		case id:
			webElement = driver.findElement(By.id(locator.getElement()));
			break;
		case cssSelector:
			webElement = driver.findElement(By.cssSelector(locator.getElement()));
			break;
		case name:
			webElement = driver.findElement(By.name(locator.getElement()));
			break;
		case className:
			webElement = driver.findElement(By.className(locator.getElement()));
			break;
		case linkText:
			webElement = driver.findElement(By.linkText(locator.getElement()));
			break;
		case partialLinkText:
			webElement = driver.findElement(By.partialLinkText(locator.getElement()));
			break;
		case tagName:
			webElement = driver.findElement(By.tagName(locator.getElement()));
			break;
		default:
			webElement = driver.findElement(By.xpath(locator.getElement()));
			break;
		}
		return webElement;
	}

	public void url(String value) {
		// 封装打开地址
		try {
			driver.get(value);
			driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
			log.info("打开浏览器，访问" + value + "网址!");
		} catch (Exception e) {
			log.error("打开浏览器，访问失败" + value + "网址!");
			ScreenShot screenShot = new ScreenShot(driver);
			// 设置截图名字
			Date nowDate = new Date();
			screenShot.setscreenName(this.formatDate(nowDate));
			screenShot.takeScreenshot();
		}
	}

	public String pageSource() {
		// 封装获取页面源码
		return driver.getPageSource();
	}

	/**
	 * @Title: quit @Description: 封装退出driver @param 参数 @return void 返回类型 @throws
	 */
	/***
	 * 关闭浏览器窗口
	 */
	public void close() {
		try {
			driver.close();
			log.info("关闭浏览器窗口");
		} catch (Exception e) {
			log.error(e + "找不到驱动" + driver);
		}
	}

	/**
	 * 退出浏览器
	 */
	public void quit() {
		try {
			driver.quit();
			log.info("退出浏览器");
		} catch (Exception e) {
			log.error(e + "找不到驱动" + driver);
		}
	}

	/**
	 * 浏览器前进
	 */
	public void forward() {
		driver.navigate().forward();
		log.info("浏览器前进");
	}

	/**
	 * 浏览器后退
	 */
	public void back() {
		driver.navigate().back();
		log.info("浏览器后退");
	}

	/**
	 * 刷新浏览器
	 */
	public void refresh() {
		driver.navigate().refresh();
		log.info("浏览器刷新");
	}

	public void click(Locator locator) {
		// 封装若IE测试则点击使用js点击
		WebElement webElement;
		try {
			webElement = findElement(locator);
			if (driver.toString().contains("InternetExplorerDriver")) {
				clickJS(locator);
			} else {
				webElement.click();
				log.info("找到元素，click成功:[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			log.error("找不到元素，click失败:[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
			e.printStackTrace();
		}
	}

	public void drag(Locator locator) {
		// 封装拖动滚动条到指定位置
		WebElement webElement = findElement(locator);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", webElement);
	}

	public void bottom() {
		// 封装拖动滚动条到最底部
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	public void input(Locator locator, String value) {
		try {
			WebElement webElement = findElement(locator);
			webElement.sendKeys(value);
			log.info("输入input值:" + "[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("输入input值失败:" + "[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
		}
		// 封装输入
	}

	public void inputClear(Locator locator, String value) {
		// 封装先清除、再输入
		WebElement element = findElement(locator);
		clear(locator);
		input(locator, value);
	}

	public void clear(Locator locator) {
		try {
			WebElement element = findElement(locator);
			element.clear();
			log.info("清除input值:" + "[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("清除input值失败:[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
		}
	}

	public void inputBox(Locator locator, String value) {
		// 封装先点击、再全选、再退格、再输入
		try {
			WebElement element = findElement(locator);
			click(locator);
			element.sendKeys(Keys.CONTROL, "a");
			element.sendKeys(Keys.BACK_SPACE);
			element.sendKeys(value);
			log.info("先点击、再全选、再退格、再输入" + "[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("先点击、再全选、再退格、再输入失败" + "[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
		}
	}

	public void select(Locator locator, String value) {
		// 封装选择下拉框（ByValue）
		try {
			Select select = new Select(findElement(locator));
			select.selectByValue(value);
			log.info("选择下拉框" + "[" + "By." + locator.getBy() + ":" + locator.getElement() + "]+输入" + value);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("选择下拉框失败" + "[" + "By." + locator.getBy() + ":" + locator.getElement() + "]+输入" + value);
		}
	}

	public void hide() {
		// 封装隐式等待
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	public String text(Locator locator) {
		// 封装获取文本值
		String text = "";
		try {
			WebElement webElement = findElement(locator);
			text = webElement.getText();
			log.info("获取文本值" + text);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取文本值失败");
		}
		return text;
	}

	public String attribute(Locator locator) {
		// 封装获取输入框提示语
		return findElement(locator).getAttribute("placeholder");
	}

	public String value(Locator locator) {
		// 封装获取输入框的值
		return findElement(locator).getAttribute("value");
	}

	public String style(Locator locator) {
		// 封装获取输入框的状态
		return findElement(locator).getAttribute("style");
	}

	public void load(Locator locator) {
		// 封装等待页面元素加载完成
		BasePage a = new BasePage();
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOf(a.findElement(locator)));
			log.info("等待页面元素加载完成");
		} catch (Exception e) {
			e.printStackTrace();
			log.info("等待页面元素未加载完成");
		}
	}

	public void loadForClick(Locator locator) {
		// 封装在页面上可用和可被单击
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(findElement(locator)));
	}

	public void loadForCustom(Locator locator) {
		// 封装自定义的显示等待
		WebElement webElement = (new WebDriverWait(driver, 30)).until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver driver) {
				WebElement element = null;
				element = getElement(locator);
				return element;
			}
		});
	}

	/**
	 * @Title: newtab @Description: 新建标签 @param @param url 链接 @return void
	 *         返回类型 @throws
	 */
	public void newtab(String url) {
		String js = "window.open('" + url + "')";
		((JavascriptExecutor) driver).executeScript(js);
		log.info("新建标签，访问" + url);
		sleep(2);
	}
	
	
	/**
	* @Title: js
	* @Description: 重写js执行方法
	* @param @param js    参数
	* @return void    返回类型
	* @throws
	*/
	public void js(String js) {
		try {
		Object a=((JavascriptExecutor) driver).executeScript(js);
		}catch(Exception e) {
			log.error("js执行失败");
		}
	}

	/**
	 * @Title: switchtab @Description: 切换标签 @param @param to 参数 @return void
	 *         返回类型 @throws
	 */
	public void switchtab(int to) {
		Set<String> winHandels = driver.getWindowHandles(); // 得到当前窗口的set集合
		List<String> it = new ArrayList<String>(winHandels); // 将set集合存入list对象
		driver.switchTo().window(it.get(to));
		String handle = driver.getWindowHandle();
		// 获取所有句柄，循环判断是否等于当前句柄
		if (it.get(to).equals(handle)) {
			log.info("成功切换到" + to);
		} else {
			log.error("失败切换到" + to);
		}
		sleep(2);
	}

	public void windows() {
		// 封装切换窗口句柄
		Set<String> winHandels = driver.getWindowHandles(); // 得到当前窗口的set集合
		List<String> it = new ArrayList<String>(winHandels); // 将set集合存入list对象
		driver.switchTo().window(it.get(1)); // 切换到弹出的新窗口
	}

	public void iframe(int value) {
		// 封装进入id=value的frame中
		try {
			driver.switchTo().frame(value);
			log.info("进入id=value的iframe成功:" + value);
		} catch (Exception e) {
			log.error("进入id=value的iframe失败:" + value);
			e.printStackTrace();
		}
	}

	public void iframe(String value) {
		// 封装进入id=value的frame中
		try {
			driver.switchTo().frame(value);
			log.info("进入id=value的iframe成功:" + value);
		} catch (Exception e) {
			log.error("进入id=value的iframe失败:" + value);
			e.printStackTrace();
		}
	}

	public void iframeNo(Locator locator) {
		// 封装进入没有id的frame中
		try {
			WebElement frame = findElement(locator);
			driver.switchTo().frame(frame);
		} catch (Exception e) {
			log.error("无法获取iframe对象:[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
			e.printStackTrace();
		}
	}
	public void dialog() {//Alert弹框确认
		try {
			driver.switchTo().alert().accept();
			log.error("进入弹框确认成功" );
		} catch (Exception e) {
			log.error("进入弹框确认失败" );
			e.printStackTrace();
		}
	}

	public void iframeback() {
		// 封装跳出iframe，进入上一层
		try {
			log.info("跳回上一层iframe成功");
			driver.switchTo().parentFrame();
		} catch (Exception e) {
			log.error("跳回上一层iframe失败");
			e.printStackTrace();
		}
	}

	public void iframeOut() {
		// 封装跳出iframe，进入default content
		try {
			System.out.println(driver.switchTo().defaultContent());
			log.info("跳出iframe成功");
		} catch (Exception e) {
			log.error("跳出iframe失败");
			e.printStackTrace();
		}
	}

	public void upFile(Locator locator, String value) {
		// 封装普通上传图片
		try {
			findElement(locator).sendKeys(value);
		} catch (Exception e) {
			log.error("无法获取对象，上传图片失败:[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
			e.printStackTrace();
		}
	}

	public void suspend(Locator locator) {
		// 封装鼠标悬浮
		try {
			Actions action = new Actions(driver);
			action.moveToElement(findElement(locator)).perform();
			log.info("获取对象，鼠标悬浮:[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
		} catch (Exception e) {
			log.error("无法获取对象，鼠标悬浮失败:[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
			e.printStackTrace();
		}
	}

	public void actionClick(Locator locator) {
		// 封装移动鼠标点击
		try {
			WebElement webElement = findElement(locator);
			Actions builder = new Actions(driver);
			Action mouserOverlogin = builder.moveToElement(webElement).build();
			mouserOverlogin.perform();
			click(locator);
		} catch (Exception e) {
			log.error("无法获取对象，鼠标移动点击失败:[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
			e.printStackTrace();
		}
	}

	public void doubleClick(Locator locator) {
		// 封装鼠标双击
		try {
			Actions action = new Actions(driver);
			action.doubleClick(findElement(locator));
			log.info("双击成功:[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
		} catch (Exception e) {
			log.error("无法找到对象，双击失败:[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
			e.printStackTrace();
		}
	}

	public void clickJS(Locator locator) {
		// 封装使用JS点击解决ie不能点击
		try {
			WebElement webElement = findElement(locator);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", webElement);
			log.info("点击:" + "[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
		} catch (Exception e) {
			log.error("点击失败:" + "[" + "By." + locator.getBy() + ":" + locator.getElement() + "]");
			e.printStackTrace();
		}
	}

	public String randomString(int length) {
		// 封装随机生成数字
		String str = "0123456789";
		// 含有数字的字符串
		Random random = new Random();// 随机类初始化
		StringBuilder sb = new StringBuilder();// StringBuffer类生成，为了拼接字符串

		for (int i = 0; i < length; ++i) {
			int number = random.nextInt(10);// [0,10)
			sb.append(str.charAt(number));
		}
		return sb.toString();
	}

	public String randomEnglish(int length) {
		// 封装随机生成大写字母
		String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random random = new Random();
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < length; ++i) {
			int number = random.nextInt(26);
			sb.append(str.charAt(number));
		}
		return sb.toString();
	}

	public void pull(Locator locator_1, Locator locator_2) {
		// 封装拖拽
		try {
			WebElement element = findElement(locator_1);
			WebElement target = findElement(locator_2);
			(new Actions(driver)).dragAndDrop(element, target).perform();
			log.info("拖拽成功:[" + "By." + locator_1.getBy() + ":" + locator_1.getElement() + "]");
			log.info("拖拽成功:[" + "By." + locator_2.getBy() + ":" + locator_2.getElement() + "]");
		} catch (Exception e) {
			log.error("拖拽失败:[" + "By." + locator_1.getBy() + ":" + locator_1.getElement() + "]");
			log.error("拖拽失败:[" + "By." + locator_2.getBy() + ":" + locator_2.getElement() + "]");
			e.printStackTrace();
		}
	}

	/**
	 * 显式等待，程序休眠暂停
	 * 
	 * @param time 以秒为单位
	 */
	public void sleep(long time) {
		try {
			Thread.sleep(time * 1000);
			log.info("等待"+time+"秒");
		} catch (InterruptedException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}
}

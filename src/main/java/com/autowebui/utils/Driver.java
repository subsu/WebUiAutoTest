package com.autowebui.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * @ClassName: Driver
 * @Description: 浏览器初始化，使用单例模式实现并行测试
 * @author Suson
 * @date 2020年1月23日
 *
 */
public class Driver {
	private static WebDriver driver = null;
	public static String osName = System.getProperties().getProperty("os.name");
	// 通过java获取当前操作系统

	public WebDriver getWebDriver(String type) {
		if (osName.equals("Linux")) {
			if (type.equals("chrome")) {
				System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\driver\\chromedriver");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("disable-infobars");
				this.driver = new ChromeDriver(options);
				this.driver.manage().window().maximize();
				return this.driver;
			} else if (type.equals("firefox")) {
				System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\driver\\geckodriver");
				System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");//关闭驱动的日志
				this.driver = new FirefoxDriver();
				this.driver.manage().window().maximize();
				return this.driver;
			} else if (type.equals("ie")) {
				System.setProperty("webdriver.ie.driver", "src\\main\\resources\\driver\\IEDriverServer");
				this.driver = new InternetExplorerDriver();
				this.driver.manage().window().maximize();
				return this.driver;
			} else if (type.equals("edge")) {
				System.setProperty("webdriver.Edge", "src\\main\\resources\\driver\\msedgedriver");
				this.driver = new EdgeDriver();
				this.driver.manage().window().maximize();
				return this.driver;
			}
		} else {
			if (type.equals("chrome")) {
				System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\driver\\chromedriver.exe");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("disable-infobars");
				this.driver = new ChromeDriver(options);
				this.driver.manage().window().maximize();
				return this.driver;
			} else if (type.equals("firefox")) {
				System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\driver\\geckodriver.exe");
				System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");//关闭驱动的日志
				this.driver = new FirefoxDriver();
				this.driver.manage().window().maximize();
				return this.driver;
			} else if (type.equals("ie")) {
				System.setProperty("webdriver.ie.driver", "src\\main\\resources\\driver\\IEDriverServer.exe");
				this.driver = new InternetExplorerDriver();
				this.driver.manage().window().maximize();
				return this.driver;
			} else if (type.equals("edge")) {
				System.setProperty("webdriver.edge.driver", "src\\main\\resources\\driver\\msedgedriver.exe");
				this.driver = new EdgeDriver();
				this.driver.manage().window().maximize();
				return this.driver;
			}
		}

		return this.driver;
	}
}

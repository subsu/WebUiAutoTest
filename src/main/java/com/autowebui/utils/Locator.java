package com.autowebui.utils;

/**
 * @ClassName: Locator
 * @Description: 公用获取对象
 * @author Suson
 * @date 2020年1月22日
 *
 */
public class Locator {
	private String element;
	private int waitSec;

	public enum ByType {
		xpath, id, linkText, name, className, cssSelector, partialLinkText, tagName
	}

	private ByType byType;

	public Locator() {
	}

	/**
	 * defaut Locator ,use Xpath
	 * 
	 * @author Suson
	 * @param element
	 */

	public Locator(String element, ByType byType) {
		this.waitSec = 2;
		this.element = element;
		this.byType = byType;
	}

	public Locator(String element, int waitSec, ByType byType) {
		this.waitSec = waitSec;
		this.element = element;
		this.byType = byType;
	}

	public String getElement() {
		return element;
	}

	public int getWaitSec() {
		return waitSec;
	}

	public ByType getBy() {
		return byType;
	}

	public void setBy(ByType byType) {
		this.byType = byType;
	}
}

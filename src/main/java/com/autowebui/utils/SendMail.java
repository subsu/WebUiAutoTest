package com.autowebui.utils;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import java.security.Security;
import java.util.ArrayList;
import com.autowebui.utils.Log;

public class SendMail {
	private Log log=new Log(SendMail.class);
	public static void main(String[] args) {
		SendMail sendMail=new SendMail();
		sendMail.sendmessage("test@163.com", "test123456", "smtp.163.com", "25", false,"test@163.com","test","test@qq.com", "你好", "你在家吗？","");

	}
	/**
	 *
	 * @param userName  发送邮箱账号  xxx@xxx.com形式
	 * @param passWord  发送邮件密码
	 * @param smtpHost  stmp服务器地址
	 * @param smtpPort  smtp服务器端口
	 * @param ssl  		ssl连接
	 * @param from      发件人地址
	 * @param tos       收件人地址
	 * @param title     标题
	 * @param content   内容
	 */
	public void sendmessage(String userName,String passWord,String smtpHost,String smtpPort,boolean ssl,String from,String fromName,String tos ,String title,String content,String fileUrl)
	{
		Properties smtpProper;
		if(ssl) {
			smtpProper=setSmtpSSL(smtpHost, smtpPort, userName, passWord);
		}else {
			smtpProper=setSmtp(smtpHost, smtpPort, userName, passWord);
		}
		smtpProper=setSmtpSSL(smtpHost, smtpPort, userName, passWord);
		Auth authenticator=new Auth(userName, passWord);
		try {

			//创建session实例
			Session session=Session.getInstance(smtpProper, authenticator);
			MimeMessage message=new MimeMessage(session);
			session.setDebug(false);

			//设置from发件人邮箱地址
			message.setFrom(new InternetAddress (from, fromName, "UTF-8"));
			//收件人to LIST
			ArrayList<Address> toList=new ArrayList<Address>();
			//收件人字符串通过,号分隔收件人
			String[] toArray=tos.split(",");
			for (int i = 0; i < toArray.length; i++)
			{
				String str = toArray[i];
				toList.add(new InternetAddress(str));
			}
			//将收件人列表转换为收件人数组
			Address[] addresses=new Address[toList.size()];
			addresses=toList.toArray(addresses);
			//设置to收件人地址
			message.setRecipients(MimeMessage.RecipientType.TO,addresses);
			//设置邮件标题
			message.setSubject(title);
			// 向multipart对象中添加邮件的各个部分内容，包括文本内容和附件
			Multipart multipart = new MimeMultipart();
			MimeBodyPart attachment = new MimeBodyPart();
			// 设置邮件的文本内容
			BodyPart contentPart = new MimeBodyPart();
			contentPart.setContent(content, "text/html;charset=UTF-8");
			multipart.addBodyPart(contentPart);
	        // 读取本地文件
	        DataHandler file = new DataHandler(new FileDataSource(fileUrl));
	        // 将附件数据添加到"节点"
	        attachment.setDataHandler(file);
	        // 设置附件的文件名（需要编码）
	        attachment.setFileName(MimeUtility.encodeText(file.getName()));
	        multipart.addBodyPart(attachment);
	        message.setContent(multipart);
			//Transport.send(message);
			Transport transport=session.getTransport();
			transport.connect(smtpHost,userName, passWord);
			transport.sendMessage(message,addresses);
			log.info("发送邮件成功！");

		} catch (Exception e) {
			// TODO: handle exception
			log.error("发送邮件失败！");
			e.printStackTrace();
		}

	}

	private Properties setSmtp(String smtpHost,String smtpPort,String userName,String passWord)
	{
		//创建邮件配置文件
		Properties maiProperties=new Properties();
		//配置smtp发件服务器
		maiProperties.put("mail.transport.protocol", "smtp");
		//配置smtp服务器
		maiProperties.put("mail.smtp.host", smtpHost);
		//配置smtp服务器端口
		maiProperties.put("mail.smtp.port", smtpPort);
		//配置smtp用户名
		maiProperties.put("mail.user", userName);
		//配置smtp身份验证
		maiProperties.put("mail.smtp.auth", "true");
		return maiProperties;
	}
	
	private Properties setSmtpSSL(String smtpHost,String smtpPort,String userName,String passWord)
	{
		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		//创建邮件配置文件
		Properties maiProperties=new Properties();
		//配置smtp发件服务器
		maiProperties.put("mail.transport.protocol", "smtp");
		//开启ssl
		maiProperties.put("mail.smtp.ssl.enable", true);
		maiProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		maiProperties.put("mail.smtp.socketFactory.fallback", "false");
		//配置smtp服务器
		maiProperties.put("mail.smtp.host", smtpHost);
		//配置smtp SSL服务器端口
		maiProperties.put("mail.smtp.socketFactory.port", smtpPort);
		//配置smtp用户名
		maiProperties.put("mail.user", userName);
		//配置smtp身份验证
		maiProperties.put("mail.smtp.auth", "true");
		return maiProperties;
	}
}
//smtp 身份验证类
class Auth extends Authenticator
{
	Properties pwdProperties;
	//构造函数

	public Auth(String userName,String passWord)
	{
		pwdProperties=new Properties();
		try {
			pwdProperties.put(userName,passWord);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	//必须实现 PasswordAuthentication 方法
	public PasswordAuthentication passwordAuthentication()
	{
		String userName=getDefaultUserName();
		//当前用户在密码表里
		if (pwdProperties.containsKey(userName)) {
			//取出密码，封装好后返回
			return new PasswordAuthentication(userName, pwdProperties.getProperty(userName).toString());

		}
		else {
			//如果当前用户不在密码表里就返回Null
			System.out.println("当前用户不存在");
			return null;


		}

	}


}
 
 

package com.autowebui.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class test {
	public static String readToString(String fileName) {
		String fileContent ="";
		  try { 
		   File f = new File(fileName);
		   if(f.isFile()&&f.exists()){
		    InputStreamReader read = new InputStreamReader(new FileInputStream(f),"utf-8");
		    BufferedReader reader=new BufferedReader(read);
		    String line;
		    while ((line = reader.readLine()) != null) {
		     fileContent += line+"\n";
		    }
		    read.close();
		   }
		  } catch (Exception e) {
		   System.out.println("读取文件内容操作出错");
		   e.printStackTrace();
		  }
		  return fileContent;
	}
	public static void main(String[] arg) {
		String a=readToString("G:\\Temp\\webui\\test-output\\2020-03-23 235546979report.html");
		System.out.println(a);
	}
}

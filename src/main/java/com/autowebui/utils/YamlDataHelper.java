package com.autowebui.utils;

import org.testng.annotations.DataProvider;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class YamlDataHelper {

	private static List<List<Object>> getYamlList(String src) {
		List<List<Object>> list = new ArrayList<List<Object>>();
		Map<String, Map<String, Object>> map = readYamlUtil(src);
		for (Map.Entry<String, Map<String, Object>> me : map.entrySet()) {
			Map<String, Object> numNameMapValue = me.getValue();
			List<Object> tmp = new ArrayList<Object>();
			for (Map.Entry<String, Object> nameMapEntry : numNameMapValue.entrySet()) {
				Object nameValue = nameMapEntry.getValue();
				tmp.add(nameValue);
			}
			list.add(tmp);
		}
		return list;
	}

	public static Map<String, Map<String, Object>> readYamlUtil(String src) {
		Map<String, Map<String, Object>> map = null;
		try {
			Yaml yaml = new Yaml();
			File dir = new File("src/" + src);
			// 获取yaml文件中的配置数据，然后转换为Map
			map = yaml.load(new FileInputStream(dir));
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;

	}

	/**
	 * @Title: yamlData @Description: yaml数据驱动生成 @param @param src
	 * 文件路径 @param @return files 生成的数据 @return Object[][] 返回类型 @throws
	 */
	public static Object[][] yamlData(String src) {
		List<List<Object>> yamlList = getYamlList(src);
		Object[][] files = new Object[yamlList.size()][];
		for (int i = 0; i < yamlList.size(); i++) {
			files[i] = new Object[] { yamlList.get(i) };
			List m = yamlList.get(i);
			files[i] = new Object[m.size()];
			for (int j = 0; j<m.size(); j++) {
					files[i][j] = m.get(j);
			}
		}
		return files;
	}

	public static void main(String[] arg) {
		YamlDataHelper a = new YamlDataHelper();
		Object[][] aa = a.yamlDataMethod();
		System.out.println(Arrays.toString(aa[0]));
	}

	/**
	 * @Title: yamlDataMethod @Description: 数据驱动样例 @param @return 测试数据 参数 @return
	 * Object[][] 返回类型 @throws
	 */
	@DataProvider(name = "yamlDataMethod")
	public Object[][] yamlDataMethod() {
		List<List<Object>> yamlList = getYamlList("test/java/com/autowebui/webui/Login/login.yaml");
		Object[][] files = new Object[yamlList.size()][];
		for (int i = 0; i < yamlList.size(); i++) {
			files[i] = new Object[] { yamlList.get(i) };
			List m = yamlList.get(i);
			files[i] = new Object[m.size()];
			for (int j = 0; j<m.size(); j++) {
					files[i][j] = m.get(j);
			}
		}
		return files;
	}
}
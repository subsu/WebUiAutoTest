package com.autowebui.PageAction;

import com.autowebui.PageObject.LoginPage;
import com.autowebui.utils.BasePage;

public class LoginAction extends BasePage{
	LoginPage login=new LoginPage();
	public LoginAction(String type, String url,String username,String psw) {
		browser(type);//打开浏览器
		sleep(3);
		url(url);//访问页面
		inputClear(login.username(),username);//清除并输入用户名
		inputClear(login.psw(),psw);//清除并输入密码
		click(login.login());//登陆
	}
	public String name() {//获取登陆后用户名
		String name=text(login.name());
		return name;
	}
}

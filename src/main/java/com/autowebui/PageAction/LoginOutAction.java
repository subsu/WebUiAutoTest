package com.autowebui.PageAction;

import com.autowebui.utils.BasePage;
import com.autowebui.PageObject.LoginOutPage;

public class LoginOutAction extends BasePage{
	LoginOutPage out=new LoginOutPage();
	public LoginOutAction() {
		suspend(out.menu());
		sleep(1);
		actionClick(out.loginOut());
	}
	public String Page() {
		String a=text(out.page());
		return a;
	}
}
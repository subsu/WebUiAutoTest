package com.autowebui.PageAction;

import com.autowebui.utils.BasePage;
import com.autowebui.PageObject.RolePage;

public class RoleAction extends BasePage {
	RolePage page = new RolePage();

	public RoleAction() {
		sleep(3);
		click(page.meun());
		sleep(3);
		click(page.meun_role());
		iframeNo(page.iframe1());
		load();
	}

	public void load() {// 等待页面加载
		load(page.load());
	}

	public void add(String name, String power, String order, boolean play, String ps, String menu) {
		click(page.add());
		iframeNo(page.iframe2());
		load(page.name());
		input(page.name(), name);// 角色名称
		input(page.power(), power);// 权限字符
		input(page.order(), order);// 显示顺序
		if (!play) {
			click(page.play());
		}
		input(page.ps(), ps);// 备注
		String[] m = menu.split(",");
		for (int i = 0; i < m.length; i++) {
			switch (m[i]) {
			case "1":
				click(page.meun_mgr());
				break;
			case "2":
				click(page.meun_mir());
				break;
			case "3":
				click(page.meun_tool());
				break;
			default:
				;
			}
		}
	}
	public void submit() {//提交
		iframeback();
		click(page.submit());
	}
	public void close() {//关闭
		iframeback();
		click(page.closed());
	}
	public void del(String val,boolean a) {
		input(page.search(),val);
		click(page.sbtn());
		sleep(1);
		click(page.delete());
		load(page.cbtn());
		if(a) {
			load(page.cbtn());
			click(page.cbtn());
		}else {
			load(page.cabtn());
			click(page.cabtn());
		}
	}
}
